<?php

namespace App\Event;

use Doctrine\DBAL\Exception\ConnectionException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class ExceptionListener
{
     public function onException(ExceptionEvent $event)
     {
        $exception = $event->getThrowable();

        if ($exception instanceof NotEncodableValueException ) { 
            $response = new JsonResponse([ 'code' => Response::HTTP_UNSUPPORTED_MEDIA_TYPE, 'message' => 'The json format is not correct'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
            $event->setResponse($response);
        }
        if ($exception instanceof VerifyEmailExceptionInterface ) { 
            $response = new JsonResponse([ 'code' => Response::HTTP_UNAUTHORIZED, 'message' => $exception->getReason()],Response::HTTP_UNAUTHORIZED);
            $event->setResponse($response);
        }

        if ($exception instanceof NotFoundHttpException ) {
            $response = new JsonResponse(['code' => Response::HTTP_NOT_FOUND,'message' => ($this->getSubErrorMessage($exception->getMessage()))],Response::HTTP_NOT_FOUND);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
            $event->setResponse($response);
        } 
        if ($exception instanceof AccessDeniedHttpException ) {
            $response = new JsonResponse(['code' => Response::HTTP_FORBIDDEN,'message' => ($this->getSubErrorMessage($exception->getMessage()))],Response::HTTP_FORBIDDEN);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
            $event->setResponse($response);
        }
    }

    private static function getSubErrorMessage($message){
        if(str_contains($message,'App\Entity')){
            $message = ' ' . $message;
            $ini = strpos($message, 'App\Entity\\');
            if ($ini == 0) return '';
            $ini += strlen('App\Entity\\');
            $len = strpos($message, ' by', $ini) - $ini;
            $message = substr($message, $ini, $len);
        }
       $message = str_replace(['"',''],'',$message);
        return $message;
    }
}
