<?php

namespace App\Entity;

use App\Repository\AdvertisementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AdvertisementRepository::class)
 */
class Advertisement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     *  @Assert\NotNull
     * @Assert\Length(min=3)
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * p
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(min=5)
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @ORM\Column(type="datetime")
     */
    private $publishAt;

    /**
     * @Assert\NotNull
     * @ORM\Column(type="string", length=255)
     */
    private $simple_description;
    
    /**
     * @Assert\NotNull
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Garage", inversedBy="advertisements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $garage;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Car", orphanRemoval=true)
     */
    private $car;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Photo", mappedBy="advertisement", orphanRemoval=true,cascade={"persist"})
     */
    private $photos;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getPublishAt(): ?\DateTimeInterface
    {
        return $this->publishAt;
    }

    public function setPublishAt(\DateTimeInterface $publishAt): self
    {
        $this->publishAt = $publishAt;

        return $this;
    }

    public function getSimpleDescription(): ?string
    {
        return $this->simple_description;
    }

    public function setSimpleDescription(string $simple_description): self
    {
        $this->simple_description = $simple_description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getGarage(): ?Garage
    {
        return $this->garage;
    }

    public function setGarage(?Garage $garage): self
    {
        $this->garage = $garage;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setAdvertisement($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getAdvertisement() === $this) {
                $photo->setAdvertisement(null);
            }
        }

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
