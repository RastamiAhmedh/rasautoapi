<?php

namespace App\Entity;

use App\Repository\DriverRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=DriverRepository::class)
 */
class Driver
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user"})
     */
    private $id;
    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(min=5)
     * @ORM\Column(type="string", length=255)
     * @Groups({"user"})
     */
    private $phone;

    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @ORM\Column(type="text")
     * @Groups({"user"})
     */
    private $address;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"user"})
     */
    private $addional_address;

    /**
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\City")
     * @Groups({"user"})
     */
    private $city;

     /**
     * @ORM\OneToMany(targetEntity="App\Entity\File", mappedBy="driver",cascade={"persist","remove"})
     * @Groups({"user"})
     */
    private $files;

      /**
     * @ORM\OneToMany(targetEntity="App\Entity\Car", mappedBy="driver", orphanRemoval=true,cascade={"persist"})
     * @Groups({"user"})
     */
    private $cars;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", orphanRemoval=true,cascade={"persist","remove"})
     * @Groups({"user"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Choice", mappedBy="driver", cascade={"persist", "remove"})
     */
    private $choices;

    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->cars = new ArrayCollection();
        $this->choices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddressName(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddionalAddress(): ?string
    {
        return $this->addional_address;
    }

    public function setAddionalAddress(?string $addional_address): self
    {
        $this->addional_address = $addional_address;

        return $this;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setDriver($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getDriver() === $this) {
                $file->setDriver(null);
            }
        }

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Car $car): self
    {
        if (!$this->cars->contains($car)) {
            $this->cars[] = $car;
            $car->setDriver($this);
        }

        return $this;
    }

    public function removeCar(Car $car): self
    {
        if ($this->cars->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getDriver() === $this) {
                $car->setDriver(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        $this->user->setRoles(['ROLE_DIRVER']);
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;
        $this->user->setRoles(['ROLE_DIRVER']);
        return $this;
    }

    /**
     * @return Collection|Choice[]
     */
    public function getChoices(): Collection
    {
        return $this->choices;
    }

    public function addChoice(Choice $choice): self
    {
        if (!$this->choices->contains($choice)) {
            $this->choices[] = $choice;
            $choice->setDriver($this);
        }

        return $this;
    }

    public function removeChoice(Choice $choice): self
    {
        if ($this->choices->removeElement($choice)) {
            // set the owning side to null (unless already changed)
            if ($choice->getDriver() === $this) {
                $choice->setDriver(null);
            }
        }

        return $this;
    }

}
