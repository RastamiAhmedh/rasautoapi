<?php

namespace App\Entity;

use App\Repository\ModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ModelRepository::class)
 */
class Model
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"brand"})
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @ORM\Column(type="string", length=255)
     * @Groups({"brand"})
     */
    private $name;

    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(min=3)
     * @ORM\Column(type="integer")
     * @Groups({"brand"})
     */
    private $serviceIntervalKm;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"brand"})
     */
    private $serviceIntervalMonth;

    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(min=3)
     * @ORM\Column(type="integer")
     * @Groups({"brand"})
     */
    private $timingBeltFreqencyKm;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"brand"})
     */
    private $timingBeltFreqencyYear;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand", inversedBy="models")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Car", mappedBy="model")
     */
    private $cars;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getServiceIntervalKm(): ?int
    {
        return $this->serviceIntervalKm;
    }

    public function setServiceIntervalKm(int $serviceIntervalKm): self
    {
        $this->serviceIntervalKm = $serviceIntervalKm;

        return $this;
    }

    public function getServiceIntervalMonth(): ?int
    {
        return $this->serviceIntervalMonth;
    }

    public function setServiceIntervalMonth(int $serviceIntervalMonth): self
    {
        $this->serviceIntervalMonth = $serviceIntervalMonth;

        return $this;
    }

    public function getTimingBeltFreqencyKm(): ?int
    {
        return $this->timingBeltFreqencyKm;
    }

    public function setTimingBeltFreqencyKm(int $timingBeltFreqencyKm): self
    {
        $this->timingBeltFreqencyKm = $timingBeltFreqencyKm;

        return $this;
    }

    public function getTimingBeltFreqencyYear(): ?int
    {
        return $this->timingBeltFreqencyYear;
    }

    public function setTimingBeltFreqencyYear(int $timingBeltFreqencyYear): self
    {
        $this->timingBeltFreqencyYear = $timingBeltFreqencyYear;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Car $car): self
    {
        if (!$this->cars->contains($car)) {
            $this->cars[] = $car;
            $car->setModel($this);
        }

        return $this;
    }

    public function removeCar(Car $car): self
    {
        if ($this->cars->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getModel() === $this) {
                $car->setModel(null);
            }
        }

        return $this;
    }
}
