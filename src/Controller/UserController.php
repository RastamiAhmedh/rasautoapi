<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('api/users')]
class UserController extends AbaseController
{

    protected $userRepository;
    protected $serializer;
    protected $entityManager;
    protected $verifyEmailHelper;

    public function __construct(
        UserRepository $userRepositoy,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder,
    ) {
        $this->userRepository = $userRepositoy;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'user_new', methods: ['POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager,SerializerInterface $serializer, ValidatorInterface $validator, UserPasswordEncoderInterface $passwordEncoder): JsonResponse
    {
        $user = new User();
        $jsonData = $request->getContent();

        /** @var User */
        $user = $serializer->deserialize($jsonData,User::class,'json');
        $user->setPassword($passwordEncoder->encodePassword($user,$user->getPassword()));
        $user->setRoles(['ADMIN']);
        $errors = $validator->validate($user);

        if(count($errors)>0){
            return $this->json($this->getInvalidFields($errors),JsonResponse::HTTP_BAD_REQUEST);;
       }

       $entityManager->persist($user);
       $entityManager->flush();

        return $this->json($user,201);
    }

    #[Route('/', name: 'users_list', methods: ['GET'])]
    public function list(UserRepository $userRepository): JsonResponse
    {
        $users = $userRepository->findAll();
        return $this->json($users,200);
    }

    #[Route('/{id}', name: 'user_show', methods: ['GET'])]
    public function show(User $user): JsonResponse
    {
        if (! $user instanceof User){
            return $this->json(['status'=> 'NOT_FOUND','message'=>'user not fount'],400);
        }
        return $this->json($user,200);
    }

    #[Route('/{id}', name: 'user_update', methods: ['PUT'])]
    public function edit(Request $request, User $user,EntityManagerInterface $entityManager,SerializerInterface $serializer, ValidatorInterface $validator,UserPasswordEncoderInterface $passwordEncoder): JsonResponse
    {
        if (!$user instanceof User){
            return $this->json(['statud'=> 'NOT_FOUND','message'=>'user not fount'],400);
        }
        $jsonData = $request->getContent();
       
        /** @var User */
        $userUpdate = $serializer->deserialize($jsonData,User::class,'json');
           
        if($userUpdate->getPassword()!=null){
            $user->setPassword($passwordEncoder->encodePassword($user,$user->getPassword()));
        }          
        $errors = $validator->validate($user);
        if (count($errors) > 0) {
            return $this->json($errors,400);
        }
        $entityManager->flush();

        return $this->json($user,200);

    }

    #[Route('/{id}', name: 'user_delete', methods: ['DELETE'])]
    public function delete(User $user, Request $request): JsonResponse
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->json(["status"=>200, "message"=>"user delete"],200,[],[]);

    }

}
