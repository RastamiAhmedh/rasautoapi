<?php

namespace App\Controller;

use App\Entity\Driver;
use App\Form\DriverType;
use App\Repository\DriverRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

#[Route('api/drivers')]
class DriverController extends AbstractProAndDriverController
{
    protected $driverRepository;
    protected $serializer;
    protected $entityManager;
    protected $verifyEmailHelper;
    protected $mailer;

    public function __construct(
        DriverRepository $driverRepository,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder,
        VerifyEmailHelperInterface $helper, 
        MailerInterface $mailer
    ) {
        $this->driverRepository = $driverRepository;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->verifyEmailHelper = $helper;
        $this->mailer = $mailer;
        parent::__construct($validator,$passwordEncoder);
    }
    
    #[Route('/', name: 'driver_list', methods: ['GET'])]
    public function list(DriverRepository $driverRepository): Response
    {
        $drivers = $driverRepository->findAll();

        return $this->json($drivers, 200, [], $this->userGroups);
    }

    #[Route('/', name: 'driver_new', methods: ['POST'])]
    public function new(Request $request): Response
    {
        $driver = new Driver;
        $driver = $this->getRequestDataIntoObject($driver);        
        $errors = $this->getProfessionalOrDriverErrors($driver);
        if (count($errors) > 0) {
            return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
        }
        
        $this->encodeProfessionalOrDriverPassword($driver);
        
        $this->entityManager->persist($driver);
        $this->entityManager->flush();
        $this->SendEmail($driver);
        return $this->json($driver, 201, [], $this->userGroups);
    }

    #[Route('/{id}', name: 'driver_show', methods: ['GET'])]
    public function show(Driver $driver): JsonResponse
    {
        return $this->json($driver, 201, [], $this->userGroups);
    }

    #[Route('/{id}', name: 'driver_edit', methods: ['POST'])]
    public function edit(Request $request, Driver $driver): Response
    {
        $form = $this->createForm(DriverType::class, $driver);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('driver_index');
        }

        return $this->render('driver/edit.html.twig', [
            'driver' => $driver,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'driver_delete', methods: ['DELETE'])]
    public function delete(Request $request, Driver $driver): Response
    {
        if ($this->isCsrfTokenValid('delete'.$driver->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($driver);
            $entityManager->flush();
        }

        return $this->redirectToRoute('driver_index');
    }
}
