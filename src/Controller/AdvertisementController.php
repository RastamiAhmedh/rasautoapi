<?php

namespace App\Controller;

use App\Entity\Advertisement;
use App\Repository\AdvertisementRepository;
use Doctrine\DBAL\Exception\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('api/advertisements')]
class AdvertisementController extends AbstractController
{
    protected $advertisementRepository ;
    protected $passwordEncoder ;
    protected $validator ;
    protected $serializer ;
    protected $entityManager ;

    public function __construct(
        AdvertisementRepository $advertisementRepositoy,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer, 
        ValidatorInterface $validator)
        {
            $this->advertisementRepository = $advertisementRepositoy;
            $this->validator = $validator;
            $this->serializer = $serializer;
            $this->entityManager = $entityManager;
        }
    
    #[Route('/', name: 'advertisements_list', methods: ['GET'])]
    public function list(): Response
    {
        $advertisements = $this->advertisementRepository->findAll();
        return $this->json($advertisements,200,[],['groups' => 'pro']);
    }

    #[Route('/', name: 'advertisement_new', methods: ['POST'])]
    public function new(Request $request,): Response
    {

        $advertisement = new Advertisement();
        $jsonData = $request->getContent();
        /** @var Advertisement */
        $advertisement =  $this->serializer->deserialize($jsonData,Advertisement::class,'json');

        $errors = $this->validator->validate($advertisement);

        if(count($errors)>0){
            return $this->json(
                [
                    'status'=> 'DATA_NO_VALIDE',
                    'message'=>'The data received is not conform', 
                    'code' => Response::HTTP_BAD_REQUEST
                ],
                Response::HTTP_BAD_REQUEST);
       }

       $this->entityManager->persist($advertisement);
       $this->entityManager->flush();

        return $this->json($advertisement,201,[],['groups'=>'pro']);
    }

    #[Route('/{id}', name: 'advertisement_show', methods: ['GET'])]
    public function show($id): Response
    {
        $advertisement = $this->advertisementRepository->findOneBy(['id'=>$id]);
        if(! $advertisement instanceof Advertisement ){
            return $this->json(['status'=> 'NOT_FOUND','message'=>'advertisement not fount'],404);

        }
        return $this->json($advertisement,200,[],['groups' => 'pro']);
    }

    #[Route('/{id}', name: 'advertisement_edit', methods: ['PUT'])]
    public function edit(int $id,Request $request,): Response
    {

        $advertisement = $this->advertisementRepository->findOneBy(['id'=>$id]);
        

         $jsonData = $request->getContent();
        /** @var Advertisement */
        $advertisementUpdate =  $this->serializer->deserialize($jsonData,Advertisement::class,'json');
        $advertisement = $advertisementUpdate;
        $errors = $this->validator->validate($advertisement);

        if(count($errors)>0){
            return $this->json(
                [
                    'status'=> 'DATA_NO_VALIDE',
                    'message'=>'The data received is not conform', 
                    'code' => Response::HTTP_BAD_REQUEST
                ],
                Response::HTTP_BAD_REQUEST);
       }
       $this->entityManager->flush();

        return $this->json($advertisement,201,[],['groups'=>'pro']);
    }

    #[Route('/{id}', name: 'advertisement_delete', methods: ['DELETE'])]
    public function delete(Advertisement $advertisement,Request $request,): Response
    {
        if ($this->isCsrfTokenValid('delete'.$advertisement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($advertisement);
            $entityManager->flush();
        }

        return $this->json(["status"=>200, "message"=>"user delete"],200);
    }
}
