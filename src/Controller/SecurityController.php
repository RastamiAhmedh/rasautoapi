<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route(name="api_login", path="/api/login_check")
     * @return JsonResponse
     */
    public function api_login(): JsonResponse
    {
        /** @var User */
        $user = $this->getUser();
        return new JsonResponse([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    }

    #[Route("/verify", name: "registration_confirmation_email", methods: ['GET'])]
    public function verifyUserEmail(Request $request,VerifyEmailHelperInterface $helper,  EntityManagerInterface $entityManager,UserRepository $userRepository,HttpClientInterface $httpClient)
    {
        $user = $this->verifyOrActivateUser($request,$helper,$userRepository);
        $user->setVerifiedEmail(true);
        $entityManager->flush();
        return $this->redirect('http://localhost:8100/home');
    }

    #[Route("/activativation_account", name: "registration_activation_account", methods: ['GET'])]
    public function activationAccountUser(Request $request,VerifyEmailHelperInterface $helper,  EntityManagerInterface $entityManager,UserRepository $userRepository)
    {
        $user = $this->verifyOrActivateUser($request,$helper,$userRepository);
        $user->setEnabled(true);
        $entityManager->flush();
        return $this->redirect('http://localhost:8100/home');
    }

    public function verifyOrActivateUser(Request $request,VerifyEmailHelperInterface $helper, UserRepository $userRepository)
    {
        // Do not get the User's Id or Email Address from the Request object
        $id = $request->get('id');
        if (null === $id) {
            throw new NotFoundHttpException('Not found id Parametre');
        }
        /** @var User */
        $user = $userRepository->find($id);
        if(null == $user){
            throw new NotFoundHttpException("User object not found");
        }
        $helper->validateEmailConfirmation($request->getUri(), $user->getId(), $user->getEmail());
        return $user;
    }
}
