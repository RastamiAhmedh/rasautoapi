<?php

namespace App\Controller;

use App\Entity\Professional;
use App\Repository\ProfessionalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

#[Route('api/professionals')]
class ProfessionalController extends AbstractProAndDriverController
{
    protected $professionalRepository;
    protected $serializer;
    protected $entityManager;
    protected $verifyEmailHelper;
    protected $mailer;

    public function __construct(
        ProfessionalRepository $professionalRepositoy,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder,
        VerifyEmailHelperInterface $helper, 
        MailerInterface $mailer
    ) {
        $this->professionalRepository = $professionalRepositoy;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->verifyEmailHelper = $helper;
        $this->mailer = $mailer;
        parent::__construct($validator,$passwordEncoder);
    }

    #[Route('/', name: 'professionals_list', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $professionals = $this->professionalRepository->findAll();

        return $this->json($professionals, 200, [], $this->userGroups);
    }

    #[Route('/', name: 'professional_new', methods: ['POST'])]
    public function new(): JsonResponse
    {
        $professional = new Professional;
        $professional = $this->getRequestDataIntoObject($professional);        
        $errors = $this->getProfessionalOrDriverErrors($professional);
        if (count($errors) > 0) {
            return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
        }
        
        $this->encodeProfessionalOrDriverPassword($professional);
        
        $this->entityManager->persist($professional);
        $this->entityManager->flush();
        $this->SendEmail($professional);
        return $this->json($professional, 201, [], $this->userGroups);
    }

    #[Route('/{id}', name: 'professional_show', methods: ['GET'])]
    public function show(Professional $professional): JsonResponse
    {
        return $this->json($professional, 200, [], $this->userGroups);
    }

    #[Route('/{id}', name: 'professional_edit', methods: ['PUT'])]
    public function edit(Professional $professional, Request $request,): JsonResponse
    {
        /** @var Professional*/
        $professionalUpdate =  $this->getRequestDataIntoObject($professional);
        if($professional->getId() != $professionalUpdate->getId()){
            throw new  AccessDeniedException("Access Denied",403); 
        }
        if ($professionalUpdate->getSiret() != null) {
           $professional->setSiret($professionalUpdate->getSiret());
        }
        $this->updateProfessionalOrDeriverUser($professionalUpdate, $professional);
        $this->entityManager->persist($professional);
        $this->entityManager->flush();

        return $this->json($professional, 201, [], $this->userGroups);
    }

    #[Route('/{id}', name: 'professional_delete', methods: ['DELETE'])]
    public function delete(Professional $professional, Request $request,): JsonResponse
    {
        if ($this->isCsrfTokenValid('delete' . $professional->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($professional);
            $entityManager->flush();
        }

        return $this->json(["status" => 200, "message" => "user delete"], 200);
    }
}
