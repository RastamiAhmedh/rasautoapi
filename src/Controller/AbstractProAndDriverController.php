<?php

namespace App\Controller;

use App\Entity\Driver;
use App\Entity\Professional;
use Container1U7eYC3\getLexikJwtAuthentication_EncoderService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

Abstract class AbstractProAndDriverController extends AbaseController
{
    protected ValidatorInterface $validator;
    protected UserPasswordEncoderInterface $passwordEncoder;
    protected array $userGroups;
  

    protected function __construct(ValidatorInterface $validator,UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;
        $this->userGroups = ['groups' => 'user'];;
    }
    /**
     * Get errors for fields professional or Driver and her user
     */
    protected function getProfessionalOrDriverErrors($object): array
    {
        $errorsPro = $this->validator->validate($object);
        
        $errorsUser = $this->validator->validate($object->getUser());
        $invalidFieldsPro = $this->getInvalidFields($errorsPro);
        $invalidFieldsUser = $this->getInvalidFields($errorsUser);
        $errors = array_merge($invalidFieldsPro, $invalidFieldsUser);
        return $errors;
    }

    /**
     * Updte professional or diver user
     */
    protected function updateProfessionalOrDeriverUser(Professional | Driver $newObject, Professional | Driver $oldObject, ){
        return $oldObject;
    }

     /**
     * Encode the professional or driver password
     */
    protected function encodeProfessionalOrDriverPassword(Professional| Driver $object)
    {
        $object->getUser()->setPassword
            (
                $this->passwordEncoder->encodePassword
                (
                    $object->getUser(),
                    $object->getUser()->getPassword()
                )
            );
    }

    /**
     * Initialise enabled in false
     */
    protected function initializeEnabled(Professional|Driver $object)
    {
        $object->getUser()->setEnabled(false);
    }

    /**
     * Initialise verifedEmail in false
     */
    protected function initializeVerifyEmail(Professional|Driver $object)
    {
        $object->getUser()->setVerifiedEmail(false);
    }

    protected function SendEmail(Professional|Driver $object)
    {
        //Email to confirm user adresse email
        $this->sendEmailSigned(
            'registration_confirmation_email',
            $object->getUser()->getEmail(),
            'Confirmation email',
            'email/confirmation_email.html.twig',
            $object
        );

        //Email to activation account by admin
        $this->sendEmailSigned(
            'registration_activation_account',
            'admin@rasauto.com',
            'Activation account ' . $object->getUser()->getFirstname() . ' ' . $object->getUser()->getLastname(),
            'email/confirmation_account.html.twig',
            $object
        );
    }

    protected function sendEmailSigned(string $routeName,string $to,string $subject, string $template,Professional|Driver $object)
    {
        $signatureUserComponents = $this->verifyEmailHelper->generateSignature(
            $routeName,
            $object->getUser()->getId(),
            $object->getUser()->getEmail(),
            ['id' => $object->getUser()->getId()]
        );

        $email = new TemplatedEmail();
        $email->to($to);
        $email->subject($subject)->from('no-reply@rasauto.com');
        $email->htmlTemplate($template);
        $email->context(['firstname'=> $object->getUser()->getFirstname() . ' ' . $object->getUser()->getLastname(),'signedUrl' => $signatureUserComponents->getSignedUrl()]);
        
        $this->mailer->send($email);
    }

}