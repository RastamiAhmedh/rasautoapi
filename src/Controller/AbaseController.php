<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;

Abstract class AbaseController extends AbstractController
{
    /**
     * Retrieves field validation errors
     */
    public function getInvalidFields(ConstraintViolationListInterface $errors): Array
    {
        $InvalidFields = [];
        foreach ($errors as $violation) {
            $InvalidFields[$violation->getPropertyPath()]= $violation->getMessage();
        }
        return $InvalidFields;
    }

    /**
     * It retrieves the data sent to create a object
     */
    public function getRequestDataIntoObject($object)
    {
        $request = new Request;
        $jsonData = $request->getContent();
        $object =  $this->serializer->deserialize($jsonData, $object::class, 'json');
        return $object;
    }
}
