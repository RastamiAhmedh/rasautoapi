<?php

namespace App\Controller;

use App\Entity\City;
use App\Form\CityType;
use App\Repository\CityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('api/cities')]
class CityController extends AbaseController
{
    #[Route('/', name: 'city_list', methods: ['GET'])]
    public function list(CityRepository $cityRepository): Response
    {
       $cities = $cityRepository->findAll();
        return $this->json($cities,JsonResponse::HTTP_OK,[]);
    }

    #[Route('/', name: 'city_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $city = new City();
        $city = $this->getRequestDataIntoObject($city);       
        $errors = $this->getInvalidFields($city);
        if (count($errors) > 0) {
            return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->entityManager->persist($city);
        $this->entityManager->flush();

        return $this->json($city,JsonResponse::HTTP_OK,[],['groups'=> 'city']);
    }

    #[Route('/{id}', name: 'city_show', methods: ['GET'])]
    public function show(City $city): Response
    {
        return $this->render('city/show.html.twig', [
            'city' => $city,
        ]);
    }

    #[Route('/{id}/edit', name: 'city_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, City $city): Response
    {
        $form = $this->createForm(CityType::class, $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('city_index');
        }

        return $this->render('city/edit.html.twig', [
            'city' => $city,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'city_delete', methods: ['POST'])]
    public function delete(Request $request, City $city): Response
    {
        if ($this->isCsrfTokenValid('delete'.$city->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($city);
            $entityManager->flush();
        }

        return $this->redirectToRoute('city_index');
    }
}
