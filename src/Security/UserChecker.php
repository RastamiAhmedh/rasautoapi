<?php
namespace App\Security;

use App\Entity\User as AppUser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if (!$user->getEnabled()) {
            throw new CustomUserMessageAccountStatusException("Your account is not activated",[],JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$user->getVerifiedEmail()) {
            throw new CustomUserMessageAccountStatusException("Your email address is not verified",[],JsonResponse::HTTP_UNAUTHORIZED);
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        /* if ($user->getEnabled()) {
            throw new CustomUserMessageAccountStatusException("Your account is not activated",[],JsonResponse::HTTP_UNAUTHORIZED);
        }

        if (!$user->getEnabled()) {
            throw new CustomUserMessageAccountStatusException("Your email address is not verified",[],JsonResponse::HTTP_UNAUTHORIZED);
        } */
    }
}